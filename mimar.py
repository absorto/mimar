#!/usr/bin/python3
# -*- coding:utf-8 -*-

import sys
import json
from random import choice


def importaArquivo(path):
    with open(path) as f:
        arq = json.load(f)
    return arq


def getCategorias(arquivo):
    """retorna lista de categorias contida no arquivo"""
    return list(arquivo.keys())


def getCategoria(arquivo, categorias):
    """escolhe categoria de maneira randomica e retorna nome da categoria e lista de palavras"""
    c = choice(categorias)
    return c, arquivo[c]


def getCarta(cartas_categoria):
    """escolhe e retorna palavra de categoria de maneira randômica"""
    return choice(cartas_categoria)


sair = None
arquivo = importaArquivo("cartas.json")
while sair != "n":
    categorias = getCategorias(arquivo)
    nome_categoria, categoria = getCategoria(arquivo, categorias)
    carta = getCarta(categoria)
    categoria.remove(carta)
    print(f"Categoria sorteada \"{nome_categoria}\"")
    print(f"Carta sorteada \"{carta}\"")
    sair = input("Rodar novamente?.... pressione qualquer tecla para continuar e 'n' para parar ")
